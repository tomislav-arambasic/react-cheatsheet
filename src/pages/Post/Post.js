import React from 'react';
import examples from '../../assets/Examples/Examples';
import ChildrenPropExample from '../../components/Examples/ChildrenProp/ChildrenProp';
import EventHandlersExample from '../../components/Examples/EventHandlers/EventHandlers';
import StateChangeExample from '../../components/Examples/StateChange/StateChange';
import PassingMethodReferences from '../../components/Examples/PassingMethodReferences/PassingMethodReferences';
import TwoWayBindingExample from '../../components/Examples/TwoWayBinding/TwoWayBinding';
import InlineStyles from '../../components/Examples/InlineStyles/InlineStyles';
import ConditionalRenderingTernary from '../../components/Examples/ConditionalRendering-Ternary/ConditionalRendering-Ternary';
import ConditionalRenderingJSwayExample from '../../components/Examples/ConditionalRendering-JS-way/ConditionalRendering-JS-way';
import MapFunctionExample from '../../components/Examples/MapFunction/MapFunction';
import RenderListUsingStateExample from '../../components/Examples/RenderListUsingState/RenderListUsingState';
import ErrorPage from '../Error/Error';
import DynamicStylesExample from '../../components/Examples/DynamicStyles/DynamicStyles';

const Post = (props) => {

    let post = null;
    const example = examples.find(example => +example.id === +props.match.params.id);

    switch (+props.match.params.id) {
        case 1:
            post = <ChildrenPropExample />;
            break;
        case 2:
            post = <EventHandlersExample />
            break;
        case 3:
            post = <StateChangeExample />
            break;
        case 4:
            post = <PassingMethodReferences />
            break;
        case 5:
            post = <TwoWayBindingExample />
            break;
        case 6:
            post = <InlineStyles />
            break;
        case 7:
            post = <ConditionalRenderingTernary />
            break;
        case 8:
            post = <ConditionalRenderingJSwayExample />
            break;
        case 9:
            post = <MapFunctionExample />
            break;
        case 10:
            post = <RenderListUsingStateExample />
            break;
        case 11:
            post = <DynamicStylesExample />
            break;
        case 12:
           // post = < />
            break;
        case 13:
           // post = < />
            break;
        case 14:
           // post = < />
            break;
        case 15:
           // post = < />
            break;
        case 16:
           // post = < />
            break;
        case 17:
           // post = < />
            break;
        default:
            post = <ErrorPage />;
            break;
    }

    return (
        <div>
            <h1>{example.title}</h1>
            <p>{example.description}</p>
            {post}
        </div>
    );
};

export default Post;