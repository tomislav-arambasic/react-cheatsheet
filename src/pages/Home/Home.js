import React from 'react';
import examples from '../../assets/Examples/Examples';
import {Card, CardTitle, CardDescription, CardButton} from '../../components/Root/Card/Card';

const Home = () => {
    console.log(examples);

    return (
        <div className="container-fluid content-row mt-4 background-color-secondary">
            <div className="row">
                {
                    examples.map(example => (
                        <div className="col col-md-6 col-lg-4 mt-4" key={example.id}>
                            <Card key={example.id}>
                                <CardTitle title={example.title}/>
                                <CardDescription  description={example.description}/>
                                <CardButton route='/post=' id={example.id}>Check it out</CardButton>
                            </Card>
                        </div>
                    ))
                }
            </div>
        </div>
    );
};

export default Home;