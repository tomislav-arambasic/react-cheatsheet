import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';
// import Navbar from '../components/Root/Navbar';
import Home from '../pages/Home/Home';
import Post from '../pages/Post/Post';

function App() {
  return (
    <div className="App">
      {/* <Navbar/> */}

      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/post=:id" exact component={Post}/>
        </Switch>
      </BrowserRouter>
         
    </div>
  );
}

export default App;
