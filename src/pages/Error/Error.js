import React from 'react';

const ErrorPage = () => {
    return (
        <h3>Something went wrong!</h3>
    );
};

export default ErrorPage;