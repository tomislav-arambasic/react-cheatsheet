import React, { Component } from 'react';

import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

class EventHandlersExample extends Component {

    presentAlertHandler() {
        alert('Button click confirmed.');
    }

    render() {
        return (
            <div>
                <CodeHighlighter code={examples[1].code[0]}/>
                <p>List of events you can listen to: https://reactjs.org/docs/events.html#supported-events.</p>
              <button onClick={this.presentAlertHandler}>Show alert</button>
            </div>
        );
    };
};

export default EventHandlersExample;