import React, { Component } from 'react';
import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

class StateChangeExample extends Component {

    state = {
        prop1: 'First state property',
        prop2: 'Second state property'
    }

    changeTextHandler = () => {
        this.setState({
            prop1: this.state.prop1 === 'First state property' ? 'Changed first property.' : 'First state property'
        })
    }

    render() {
        return (
            <div>
                <CodeHighlighter code={examples[2].code[0]}/>

                <p>{this.state.prop1}</p>
                <p>{this.state.prop2}</p>
              <button onClick={this.changeTextHandler}>Change state - text</button>
            </div>
        );
    };
};

export default StateChangeExample;

