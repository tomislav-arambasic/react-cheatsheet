import React, { Component } from 'react';

import LoremIpsum from '../../../assets/LoremIpsum'
import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';
import { Card, CardDescription } from '../../Root/Card/Card';

class ConditionalRenderingTernary extends Component {

    state = {
        showCard: true,
    }

    toggleShowCardHandler = () => {
        this.setState({
            showCard: !this.state.showCard
        })
    }

    render() {
        return (
            <div>
                <CodeHighlighter code={examples[6].code[0]} />

                {this.state.showCard ?
                    <div>
                        <Card>
                            <CardDescription description={LoremIpsum} />
                        </Card>
                        <button onClick={this.toggleShowCardHandler}>Toggle text</button>
                    </div>
                    :
                    <div>
                        <button onClick={this.toggleShowCardHandler}>Toggle text</button>
                    </div>
                }
            </div>
        );
    };
};

export default ConditionalRenderingTernary;

