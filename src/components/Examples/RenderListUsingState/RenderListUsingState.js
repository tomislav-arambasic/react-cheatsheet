import React, { Component } from 'react';

import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

class RenderListUsingStateExample extends Component {

    state = {
        numberArray : [1, 4, 7]
    }

    DeleteNumberHandler = (numberIndex) => {
        // wrong way!
        // const numArray = this.state.numberArray;

        // right way - with no state mutation, using spread/rest operator
        const numArray = [...this.state.numberArray];
        numArray.splice(numberIndex, 1);
        this.setState({
            numberArray: numArray
        })
    }

    ResetArrayHandler= () => {
        this.setState({
            numberArray: [1,4,7]
        })
    }

    render() {
        return (
            <div>
                <CodeHighlighter code={examples[9].code[0]}/>

                {this.state.numberArray.map(number => {
                    return (
                        <div key={number}>
                            <p>I'm number {number}!</p>
                            <button onClick={() => this.DeleteNumberHandler(this.state.numberArray.indexOf(number))}>Delete number {number}</button>
                        </div>
                    )
                })}
                <button onClick={this.ResetArrayHandler}>I wanna do it again!</button>
            </div>
        );
    };
};

export default RenderListUsingStateExample;

