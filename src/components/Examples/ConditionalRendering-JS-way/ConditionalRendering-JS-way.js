import React, { Component } from 'react';

import LoremIpsum from '.././../../assets/LoremIpsum';
import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';
import { Card, CardDescription } from '../../Root/Card/Card';

class ConditionalRenderingJSwayExample extends Component {
    state = {
        showCard: true,
    }

    toggleShowCardHandler = () => {
        this.setState({
            showCard: !this.state.showCard
        })
    }

    render() {
        let card = null;

        if (this.state.showCard) {
            card = 
                <div>
                    <Card>
                        <CardDescription description={LoremIpsum} />
                    </Card>
                </div>
            
        }

        return (
            <div>
                <CodeHighlighter code={examples[7].code[0]}/>
                {card}
                <button onClick={this.toggleShowCardHandler}>Toggle text</button>
            </div>
        );
    };
};

export default ConditionalRenderingJSwayExample;

