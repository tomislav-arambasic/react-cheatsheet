import React from 'react';
import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

const InlineStyles = () => {

    const styles = {
        redBoldText : {
            color: 'red',
            font: '24px bold',
        },
        greenUnderlineLink: {
            color: 'green',
            textDecoration: 'underline'
        }
    }

    return (
        <div>
            <CodeHighlighter code={examples[5].code[0]} />
                <h4 style={styles.redBoldText}>Red bold text</h4>
                <a href='www.google.com' style={styles.greenUnderlineLink}> Google link </a>
        </div>
    );
};

export default InlineStyles;

