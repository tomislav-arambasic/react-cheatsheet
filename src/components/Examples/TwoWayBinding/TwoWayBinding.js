import React, { Component } from 'react';
import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

class TwoWayBindingExample extends Component {

    state = {
        text: 'Initial value',
    }

    changeTextHandler = (event) => {
        this.setState({
            text: event.target.value
        })
    }

    render() {
        return (
            <div>
                <CodeHighlighter code={examples[4].code[0]} />
                <p>{this.state.text}</p>
                <input onChange={this.changeTextHandler} value={this.state.text} />
            </div>
        );
    };
};

export default TwoWayBindingExample;

