import React, { Component } from 'react';

import{ Card, CardTitle, CardDescription, CardButton } from '../../../components/Root/Card/Card';
import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

class PassingMethodReferencesExample extends Component {

    state = {
        title: 'Card title'
    }

    changeTextHandler = () => {
        this.setState({
            title: this.state.title === 'Card title' ? 'Changed title' : 'Card title'
        })
    }

    render() {
        return (
            <div>
                PassingMethodReferences component passing changeTextHandler function to child CardButton component:
                <CodeHighlighter code={examples[3].code[0]}/>
                Card component:
                <CodeHighlighter code={examples[3].code[1]}/>

                <Card>
                    <CardTitle title={this.state.title} />
                    <CardDescription description="Card description" />
                    <CardButton function={this.changeTextHandler} text="Change title" />
                </Card>
            </div>
        );
    };
};

export default PassingMethodReferencesExample;

