import React from 'react';

import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

const MapFunctionExample = () => {

    const numberArray = [1,4,7];
    return (
        <div>
            <CodeHighlighter code={examples[8].code[0]}/>

            {numberArray.map(number => {
                return <p key={number}>I'm number {number}!</p>
            })}
        </div>
    );
};

export default MapFunctionExample;

