import React from 'react';
import { Card, CardTitle, CardDescription } from '../../Root/Card/Card';

import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
import examples from '../../../assets/Examples/Examples';

const ChildrenPropExample = () => {
    return (
        <div>
            <CodeHighlighter code={examples[0].code[0]}/>
            <CodeHighlighter code={examples[0].code[1]}/>
            <Card>
                <CardTitle title="I'm first child element" />
                <CardDescription description="I'm second child element" />
            </Card>
        </div>
    );
};

export default ChildrenPropExample;