import React, { Component } from 'react';
import { Card, CardButton } from '../../Root/Card/Card';
import loremIpsum from '../../../assets/LoremIpsum';
// import CodeHighlighter from '../../Root/CodeHighlighter/CodeHighlighter';
// import examples from '../../../assets/Examples/Examples';

class DynamicStylesExample extends Component {

    state = {
        defaultTitleStyle: true,
        defaultDescriptionStyle: true
    }

    changeTitleStyleHandler = () => {
        this.setState({
            defaultTitleStyle: !this.state.defaultTitleStyle
        })
    }

    changeDescriptionStyleHandler = () => {
        this.setState({
            defaultDescriptionStyle: !this.state.defaultDescriptionStyle
        })
    }

    render() {
        let styles = {
            titleStyle: {
                color: "green"
            },
            descriptionStyle: {
                color: "red"
            }
        }

        if (!this.state.defaultTitleStyle) {
            styles.titleStyle = {
                fontWeight: "bold",
                color:"blue"
            }
        }
        else {
            styles.titleStyle = {
                color: "green"
            }
        }

        if (!this.state.defaultDescriptionStyle) {
            styles.descriptionStyle = {
                fontWeight: "bold",
                color:"blue",
                marginTop: "30px"
            }
        }
        else {
            styles.descriptionStyle = {
                color: "red"
            }
        }

        return (
            <div>
                <Card>
                    <h3 style={styles.titleStyle}>Dummy title</h3>
                    <p style={styles.descriptionStyle}>{loremIpsum}</p>
                </Card>
                <CardButton click={this.changeTitleStyleHandler}>Change title style</CardButton>
                <CardButton click={this.changeDescriptionStyleHandler}>Change description style</CardButton>
            </div>
        );
    };
};

export default DynamicStylesExample;