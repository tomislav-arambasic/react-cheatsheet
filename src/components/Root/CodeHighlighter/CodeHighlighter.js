import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokai } from 'react-syntax-highlighter/dist/esm/styles/hljs';

const CodeHighlighter = (props) => {
    return (
            <SyntaxHighlighter language="javascript" style={monokai}>
                {props.code}
            </SyntaxHighlighter>
    );
};

export default CodeHighlighter;