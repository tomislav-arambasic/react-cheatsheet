import React from 'react';
import { Link } from 'react-router-dom';

import './Card.css'

export const Card = (props) => {
    return (
        <div className="text-center bg-white" id="card">
            {props.children}
        </div>
    )
}

export const CardImage = (props) => {
    return (
        <div className="card text-center">
            <img src={props.image} alt="" width="100%" />
        </div>
    )
}

export const CardTitle = (props) => {
    return (
        <div className="text-center p-4">
            <h4>{props.title}</h4>
        </div>
    )
}

export const CardDescription = (props) => {
    return (
        <div className="text-center p-4">
            <p>{props.description}</p>
        </div>
    )
}

export const CardButton = (props) => {
    let button = null;

    if (props.route) {
        button = 
        <Link to={{ pathname: props.route + props.id, }}>
            <button type="button" className="btn btn-outline-info mb-4">{props.children}</button>
        </Link>
    }
    else if (props.click) {
        button =
        <button type="button" className="btn btn-outline-info mb-4" onClick={props.click}>{props.children}</button>
    }
        return (
            <div>
                { button }
            </div>
        )
}
