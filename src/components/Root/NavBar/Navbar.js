import React from 'react';
import logo from '../../logo.svg';


const Navbar = () => {
    return (
        <nav className="navbar navbar-dark bg-white">
            <a href="/"><img src={logo} className="App-logo" alt="logo" /></a>
            <h1 className="text-info text-uppercase m-auto">React.js cheatsheet</h1>
            <form className="form-inline">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                <button className="btn btn-outline-info my-2 my-sm-0" type="button">Search</button>
            </form>
        </nav>
    );
};

export default Navbar;

