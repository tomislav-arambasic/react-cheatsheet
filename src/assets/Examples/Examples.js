const examples = [
    {
        id: 1,
        title: 'Children prop',
        description: '"Props.children is used to display whatever you include between the opening and closing tags when invoking a component."',
        code: [
            `import React from 'react';
import { Card, CardTitle, CardDescription } from '../../Root/Card/Card';

const ChildrenPropExample = () => {
    return (
        <div>
            <Card>
                <CardTitle title="I'm first child element" />
                <CardDescription description="I'm second child element" />
            </Card>
        </div>
    );
};

export default ChildrenPropExample;`,




            `import React from 'react';
import { Link } from 'react-router-dom';

import './Card.css'

export const Card = (props) => {
    return (
        <div className="text-center bg-white" id="card">
            { props.children }  
        </div>
    )
}

export const CardImage = (props) => {
    return (
        <div className="card text-center">
            <img src={props.image} alt="" width="100%"></img>
        </div>  
    )
}

export const CardTitle = (props) => {
    return (
        <div className="text-center p-4">
            <h4>{props.title}</h4>
        </div>
    )
}

export const CardDescription = (props) => {
    return (
        <div className="text-center p-4">
            <p>{props.description}</p>
        </div>
    )
}

export const CardButton = (props) => {
    return (
        <Link to={{pathname: props.route +  props.id, }}>
            <button type="button" className="btn btn-outline-info mb-4">Check it out</button>
        </Link>   
    )
}
`
        ]
    },
    {
        id: 2,
        title: 'Event handlers',
        description: 'How to use event handlers and which events you can listen to.',
        code: [
            `import React, { Component } from 'react';

class EventHandlersExample extends Component {

    presentAlertHandler = () => {
        alert('Button click confirmed.');
    }

    render() {
        return (
            <div>
                <p>List of events you can listen to: </p>
                <a href='https://reactjs.org/docs/events.html#supported-events'>'https://reactjs.org/docs/events.html#supported-events</a>
                <button onClick={this.presentAlertHandler}>Show alert</button>
            </div>
        );
    };
};

export default EventHandlersExample;`
        ]
    },
    {
        id: 3,
        title: 'State changes',
        description: 'Change rendered UI using React core concept - state and setState()',
        code: [
            `import React, { Component } from 'react';

class StateChangeExample extends Component {
    constructor() {
        super();
    }

    state = {
        prop1: 'First state property',
        prop2: 'Second state property'
    }

    changeTextHandler = () => {
        this.setState({
            prop1: this.state.prop1 === 'First state property' ? 'Changed first property.' : 'First state property'
        })
    }

    render() {
        return (
            <div>
                <p>{this.state.prop1}</p>
                <p>{this.state.prop2}</p>
              <button onClick={this.changeTextHandler}>Change state - text</button>
            </div>
        );
    };
};

export default StateChangeExample;`
        ]
    },
    {
        id: 4,
        title: 'Passing method references as props',
        description: 'Allows child components to call parent component\'s methods.',
        code: [
            `import React, { Component } from 'react';

import{ Card, CardTitle, CardDescription, CardButton } from '../../../components/Root/Card/Card';

class PassingMethodReferences extends Component {

    state = {
        title: 'Card title'
    }

    changeTextHandler = () => {
        this.setState({
            title: this.state.title == 'Card title' ? 'Changed title' : 'Card title'
        })
    }

    render() {
        return (
            <div>
                <Card>
                    <CardTitle title={this.state.title} />
                    <CardDescription description="Card description" />
                    <CardButton function={this.changeTextHandler} text="Change title" />
                </Card>
            </div>
        );
    };
};

export default PassingMethodReferences;`,






            `import React from 'react';
import { Link } from 'react-router-dom';

import './Card.css'

export const Card = (props) => {
    return (
        <div className="text-center bg-white" id="card">
            { props.children }  
        </div>
    )
}

export const CardImage = (props) => {
    return (
        <div className="card text-center">
            <img src={props.image} alt="" width="100%"/>
        </div>  
    )
}

export const CardTitle = (props) => {
    return (
        <div className="text-center p-4">
            <h4>{props.title}</h4>
        </div>
    )
}

export const CardDescription = (props) => {
    return (
        <div className="text-center p-4">
            <p>{props.description}</p>
        </div>
    )
}

export const CardButton = (props) => {
    let button = null;
    if (props.route) {
        // route button
        button = ( 
            <Link to={{pathname: props.route +  props.id, }}>
                <button type="button" className="btn btn-outline-info mb-4" >Check it out</button>
            </Link>
        );
    }
    else if (props.function) {
        // function button
        button = (
            <button type="button" className="btn btn-outline-info mb-4" onClick={props.function}>{props.text}</button>
        );
    }

    return (
        <div>
         { button }
        </div>
    )
}`
        ]
    },
    {
        id: 5,
        title: 'Two way binding',
        description: '',
        code: [
            `import React, { Component } from 'react';

class TwoWayBindingExample extends Component {

    state = {
        text: 'Initial value',
    }

    changeTextHandler = (event) => {
        this.setState({
            text: event.target.value
        })
    }

    render() {
        return (
            <div>
                <p>{this.state.text}</p>
                <input onChange={this.changeTextHandler} value={this.state.text} />
            </div>
        );
    };
};

export default TwoWayBindingExample;`
        ]
    },
    {
        id: 6,
        title: 'Inline styles',
        description: '',
        code: [
            `import React from 'react';

const InlineStyles = () => {

    const styles = {
        redBoldText : {
            color: 'red',
            font: '24px bold',
        },
        greenUnderlineLink: {
            color: 'green',
            textDecoration: 'underline'
        }
    }

    return (
        <div>
                <h4 style={styles.redBoldText}>Red bold text</h4>
                <a href='www.google.com' style={styles.greenUnderlineLink}> Google link </a>
        </div>
    );
};

export default InlineStyles;
`
        ]
    },
    {
        id: 7,
        title: 'Conditional rendering - ternary expressions',
        description: '',
        code: [
            `import React, { Component } from 'react';

import LoremIpsum from '../../../assets/LoremIpsum'
import { Card, CardDescription } from '../../Root/Card/Card';

class ConditionalRenderingTernary extends Component {

    state = {
        showCard: true,
    }

    toggleShowCardHandler = () => {
        this.setState({
            showCard: !this.state.showCard
        })
    }

    render() {
        return (
            <div>
                {this.state.showCard ?
                    <div>
                        <Card>
                            <CardDescription description={LoremIpsum} />
                        </Card>
                        <button onClick={this.toggleShowCardHandler}>Toggle text</button>
                    </div>
                    :
                    <div>
                        <button onClick={this.toggleShowCardHandler}>Toggle text</button>
                    </div>
                }
            </div>
        );
    };
};

export default ConditionalRenderingTernary;
`
        ]
    },
    {
        id: 8,
        title: 'Conditional rendering - Javascript way',
        description: '',
        code: [
            `import React, { Component } from 'react';

import LoremIpsum from '.././../../assets/LoremIpsum';
import { Card, CardDescription } from '../../Root/Card/Card';

class ConditionalRenderingJSwayExample extends Component {
    state = {
        showCard: true,
    }

    toggleShowCardHandler = () => {
        this.setState({
            showCard: !this.state.showCard
        })
    }

    render() {
        let card = null;

        if (this.state.showCard) {
            card = (
                <div>
                    <Card>
                        <CardDescription description={LoremIpsum} />
                    </Card>
                </div>
            )
        }

        return (
            <div>
                {card}
                <button onClick={this.toggleShowCardHandler}>Toggle text</button>
            </div>
        );
    };
};

export default ConditionalRenderingJSwayExample;
`
        ]
    },
    {
        id: 9,
        title: 'Map function',
        description: '',
        code: [
            `import React from 'react';

const MapFunctionExample = () => {

    const numberArray = [1,4,7];
    return (
        <div>
            {numberArray.map(number => {
                <p>I'm number {number}!</p>
            })}
        </div>
    );
};

export default MapFunctionExample;
`
        ]
    },
    {
        id: 10,
        title: 'Rendering and modifying lists using state',
        description: '',
        code: [
            `import React, { Component } from 'react';

class RenderListUsingStateExample extends Component {

    state = {
        numberArray : [1, 4, 7]
    }

    DeleteNumberHandler = (numberIndex) => {
        // wrong way!
        // const numArray = this.state.numberArray;

        // right way - with no state mutation, using spread/rest operator
        const numArray = [...this.state.numberArray];
        numArray.splice(numberIndex, 1);
        this.setState({
            numberArray: numArray
        })
    }

    ResetArrayHandler= () => {
        this.setState({
            numberArray: [1,4,7]
        })
    }

    render() {
        return (
            <div>
                {this.state.numberArray.map(number => {
                    return (
                        <div key={number}>
                            <p>I'm number {number}!</p>
                            <button onClick={() => this.DeleteNumberHandler(this.state.numberArray.indexOf(number))}>Delete number {number}</button>
                        </div>
                    )
                })}
                <button onClick={this.ResetArrayHandler}>I wanna do it again!</button>
            </div>
        );
    };
};

export default RenderListUsingStateExample;
`
        ]
    },
    {
        id: 11,
        title: 'Dynamic styles',
        description: '',
        code: [
            `
`
        ]
    },
    {
        id: 12,
        title: 'Dynamic style class names',
        description: '',
        code: [
            `
`
        ]
    },
    {
        id: 13,
        title: 'Radium package',
        description: 'Used when we want to use media queires and pseudo selectors in inline styles.',
        code: [
            `
`
        ]
    },
    {
        id: 14,
        title: 'CSS modules',
        description: 'How to enable and use CSS modules.',
        code: [
            `
`
        ]
    },
    {
        id: 15,
        title: 'CSS - Pseudo selectors and nested classes',
        description: '',
        code: [
            `
`
        ]
    },
    {
        id: 16,
        title: 'Error boundaries',
        description: '',
        code: [
            `
`
        ]
    },
    {
        id: 17,
        title: '',
        description: '',
        code: [
            `
`
        ]
    },
    {
        id: 18,
        title: '',
        description: '',
        code: [
            `
`
        ]
    },
];

export default examples;